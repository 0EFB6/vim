# Dracula for [Vim](http://www.vim.org/)

> A dark theme for [Vim](http://www.vim.org/).

![Screenshot](./screenshot.png)

## Install

All instructions can be found at [draculatheme.com/vim](https://draculatheme.com/vim).

## License

[MIT License](./LICENSE)
