" use :noh to clear previously highlighted search
" addons used - Papercolor, tabline, surround, repeat.vim

syntax sync minlines=256
set mouse=a					" use mouse
set number					" show number
set tabstop=4				" tab size 4
syntax on					" syntax highlight
set smartindent				" enable smart indention
set softtabstop=4			" tab 4
set wildmenu                " visual autocomplete for command menu
set showmatch               " highlight matching [{()}]
set incsearch               " search as characters are entered
set hlsearch                " highlight matches
set foldenable              " enable folding
set modelines=1
set autoindent              " Copy indent from last line when starting new line
set autoread                " Set to auto read when a file is changed from the outside
set expandtab
set clipboard=unnamed
set shiftwidth=4 
set smarttab	
set showtabline=4
set noshowmode
set ic
set re=1
set nofixendofline
set nocursorcolumn
set cursorline
set norelativenumber
set lazyredraw
set ttyfast
set scrolloff=3
set showtabline=4
set noshowmode
set cindent
set cinkeys-=0#
set indentkeys-=0#
set smartcase 
set timeoutlen=100
set ttimeoutlen=5

nnoremap <S-Tab> <<
inoremap <S-Tab> <C-d>

noremap <esc><esc> :noh<return><esc>
noremap <a-y> "+y
noremap <a-p> "+p

noremap <silent> <C-t> :tabnew<CR>
noremap <silent> <C-q> :q<CR>
noremap <silent> <C-F8> :q!<CR>
noremap <silent> <C-F11> :tabnext<CR>
noremap <silent> <C-F10> :tabprevious<CR>
inoremap <silent> <C-t> <Esc>:tabnew<CR>
inoremap <silent> <C-q> <Esc>:q<CR>
inoremap <silent> <C-F8> <Esc>:q!<CR>
inoremap <silent> <C-F11> <Esc>:tabnext<CR>
inoremap <silent> <C-F10> <Esc>:tabprevious<CR>

noremap <C-s> :update<CR>
noremap <C-F9> :execute 'silent! write !sudo /usr/bin/tee "%" >/dev/null' <bar> edit!<CR>
command -bar Ss execute 'silent! write !sudo /usr/bin/tee "%" >/dev/null' <bar> edit!
inoremap <C-s> <Esc>:update<CR>
inoremap <C-F9> <Esc>:execute 'silent! write !sudo /usr/bin/tee "%" >/dev/null' <bar> edit!<CR>

noremap  <F5> :vert term make run<CR>
inoremap <F5> <Esc>:vert term make run<CR>

noremap <S-F5> :vert term make<CR>
inoremap <S-F5> <Esc>:vert term make<CR>

filetype indent on
filetype plugin on
filetype plugin indent on
set t_Co=256
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_altv = 1
let g:netrw_winsize = 20
let g:netrw_browse_split=3

nmap <S-Enter> O<Esc>
nmap <CR> o<Esc>

syntax enable

" Dracula Theme
"packadd! dracula
"colorscheme dracula

" Everforest Theme
if has('termguicolors')
	set termguicolors
endif
set background=dark 
let g:everforest_background = 'medium'
let g:everforest_better_performance= 1
colorscheme everforest


set laststatus=2
autocmd BufNewFile,BufRead *.asm set syntax=nasm
inoremap kl <Esc>

let s:currentmode={
    \ 'n'  : ["%1*",' NORMAL '],
    \ 'no' : ["%1*",' NORMAL·OPERATOR PENDING '],
    \ 'v'  : ["%3*",' VISUAL '],
    \ 'V'  : ["%3*",' V·LINE '],
    \ "^V" : ["%3*",' V·BLOCK '],
    \ 's'  : ["%5*",' SELECT '],
    \ 'S'  : ["%5*",' S·LINE '],
    \ '^S' : ["%5*",' S·BLOCK '],
    \ 'i'  : ["%2*",' INSERT '],
    \ 'R'  : ["%4*",' REPLACE '],
    \ 'Rv' : ["%4*",' V·REPLACE '],
    \ 'c'  : ["%2*",' COMMAND '],
    \ 'r'  : ["%4*",' PROMPT '],
    \}

let s:file_node_extensions = {
	\ 'asm'	     : '',
	\ 'txtfmt'   : '',
    \ 'styl'     : '',
    \ 'sass'     : '',
    \ 'scss'     : '',
    \ 'htm'      : '',
    \ 'html'     : '',
    \ 'slim'     : '',
    \ 'haml'     : '',
    \ 'ejs'      : '',
    \ 'css'      : '',
    \ 'less'     : '',
    \ 'md'       : '',
    \ 'mdx'      : '',
    \ 'markdown' : '',
    \ 'rmd'      : '',
    \ 'json'     : '',
    \ 'webmanifest' : '',
    \ 'js'       : '',
    \ 'mjs'      : '',
    \ 'jsx'      : '',
    \ 'rb'       : '',
    \ 'gemspec'  : '',
    \ 'rake'     : '',
    \ 'php'      : '',
    \ 'py'       : '',
    \ 'pyc'      : '',
    \ 'pyo'      : '',
    \ 'pyd'      : '',
    \ 'coffee'   : '',
    \ 'mustache' : '',
    \ 'hbs'      : '',
    \ 'conf'     : '',
    \ 'ini'      : '',
    \ 'yml'      : '',
    \ 'yaml'     : '',
    \ 'toml'     : '',
    \ 'bat'      : '',
    \ 'jpg'      : '',
    \ 'jpeg'     : '',
    \ 'bmp'      : '',
    \ 'png'      : '',
    \ 'webp'     : '',
    \ 'gif'      : '',
    \ 'ico'      : '',
    \ 'twig'     : '',
    \ 'cpp'      : '',
    \ 'c++'      : '',
    \ 'cxx'      : '',
    \ 'cc'       : '',
    \ 'cp'       : '',
    \ 'c'        : '',
    \ 'cs'       : '',
    \ 'h'        : '',
    \ 'hh'       : '',
    \ 'hpp'      : '',
    \ 'hxx'      : '',
    \ 'hs'       : '',
    \ 'lhs'      : '',
    \ 'nix'      : '',
    \ 'lua'      : '',
    \ 'java'     : '',
    \ 'sh'       : '',
    \ 'fish'     : '',
    \ 'bash'     : '',
    \ 'zsh'      : '',
    \ 'ksh'      : '',
    \ 'csh'      : '',
    \ 'awk'      : '',
    \ 'ps1'      : '',
    \ 'ml'       : 'λ',
    \ 'mli'      : 'λ',
    \ 'diff'     : '',
    \ 'db'       : '',
    \ 'sql'      : '',
    \ 'dump'     : '',
    \ 'clj'      : '',
    \ 'cljc'     : '',
    \ 'cljs'     : '',
    \ 'edn'      : '',
    \ 'scala'    : '',
    \ 'go'       : '',
    \ 'dart'     : '',
    \ 'xul'      : '',
    \ 'sln'      : '',
    \ 'suo'      : '',
    \ 'pl'       : '',
    \ 'pm'       : '',
    \ 't'        : '',
    \ 'rss'      : '',
    \ 'f#'       : '',
    \ 'fsscript' : '',
    \ 'fsx'      : '',
    \ 'fs'       : '',
    \ 'fsi'      : '',
    \ 'rs'       : '',
    \ 'rlib'     : '',
    \ 'd'        : '',
    \ 'erl'      : '',
    \ 'hrl'      : '',
    \ 'ex'       : '',
    \ 'exs'      : '',
    \ 'eex'      : '',
    \ 'leex'     : '',
    \ 'vim'      : '',
    \ 'ai'       : '',
    \ 'psd'      : '',
    \ 'psb'      : '',
    \ 'ts'       : '',
    \ 'tsx'      : '',
    \ 'jl'       : '',
    \ 'pp'       : '',
    \ 'vue'      : '﵂',
    \ 'elm'      : '',
    \ 'swift'    : '',
    \ 'xcplayground' : '',
    \ 'tex'      : 'ﭨ',
    \ 'r'        : 'ﳒ',
    \ 'rproj'    : '鉶'
    \}


" MODIFYING STATUS BAR (displayed at bottom of VIM)
"et statusline+=%0*\ %{g:currentmode[mode()]}
"et statusline+=%1*\ %t\                                  "File+path
"et statusline+=%2*\ %y\                                  "FileType
"et statusline+=%3*\ %{''.(&fenc!=''?&fenc:&enc).''}      "Encoding
"et statusline+=%3*\ %{(&bomb?\",BOM\":\"\")}\            "Encoding2
"et statusline+=%4*\ %{&ff}\                              "FileFormat (dos/unix..) 
"et statusline+=%8*\ %=\ row:%l/%L\ (%03p%%)\             "Rownumber/total (%)
"et statusline+=%9*\ col:%03c\                            "Colnr
"et statusline+=%0*\ \ %m%r%w\ %P\ \                      "Modified? Readonly? Top/bot.
hi User1 ctermfg=235  ctermbg=154 cterm=bold  
hi User2 ctermfg=235  ctermbg=39  cterm=bold
hi User3 ctermfg=235  ctermbg=170 cterm=bold
hi User4 ctermfg=235  ctermbg=204 cterm=bold
hi User5 ctermfg=white  
hi User6 ctermfg=235  ctermbg=145 
hi User7 ctermfg=114  ctermbg=236 
function! g:Active() abort
    let l:mode = mode()
    let l:statusline = get(s:currentmode, l:mode, "%1*")[0]  
    let l:statusline.= get(s:currentmode, l:mode, " NORMAL ")[1] 
    let l:statusline.= "%#LineNr#%7*  %t  %5*%="
	let l:statusline.= "%5* [%{toupper(&filetype)} " . get(s:file_node_extensions, &filetype, '') . " ] [Arch︁] [%p%%] "
    "[Debian  ] [%p%%]
    return l:statusline
endfunction
function! s:StatusLine(active) abort
    if &buftype ==# 'nofile' || &filetype ==# 'netrw'
        " Likely a file explorer.
        setlocal statusline=%0*\ \%m\
"	setlocal statusline=
    elseif &buftype ==# 'nowrite'
        " Don't set a custom status line for certain special windows.
        return
    elseif a:active == 1
        setlocal statusline=%!g:Active()
    else
        setlocal statusline="%6* INACTIVE "
    endif
endfunction



" Minimalist-AutoCompletePop-Plugin
set completeopt=menu,menuone,noinsert
inoremap <expr> <CR> pumvisible() ? "\<C-Y>" : "\<CR>"
autocmd InsertCharPre * call AutoComplete()
fun! AutoComplete()
    if v:char =~ '\K'
        \ && getline('.')[col('.') - 4] !~ '\K'
        \ && getline('.')[col('.') - 3] =~ '\K'
        \ && getline('.')[col('.') - 2] =~ '\K' " last char
        \ && getline('.')[col('.') - 1] !~ '\K'
        call feedkeys("\<C-P>", 'n')
    end
endfun

set omnifunc=syntaxcomplete#Complete
set backspace=indent,eol,start

" Define mappings.
augroup NetrwOpenMultiTabGroup
   autocmd!
   autocmd WinEnter,BufWinEnter  * call s:StatusLine(1)
   autocmd WinLeave              * call s:StatusLine(0)
   autocmd VimEnter * highlight clear SignColumn
   autocmd BufWritePre *.php,*.py,*.js,*.txt,*.hs,*.java,*.md
               \:call <SID>StripTrailingWhitespaces()
   autocmd FileType java setlocal noexpandtab
   autocmd FileType java setlocal list
   autocmd FileType java setlocal listchars=tab:+\ ,eol:-
   autocmd FileType java setlocal formatprg=par\ -w80\ -T4
   autocmd FileType php setlocal expandtab
   autocmd FileType php setlocal list
   autocmd FileType php setlocal listchars=tab:+\ ,eol:-
   autocmd FileType php setlocal formatprg=par\ -w80\ -T4
   autocmd FileType ruby setlocal tabstop=2
   autocmd FileType ruby setlocal shiftwidth=2
   autocmd FileType ruby setlocal softtabstop=2
   autocmd FileType ruby setlocal commentstring=#\ %s
   autocmd FileType python setlocal commentstring=#\ %s
   autocmd BufEnter *.cls setlocal filetype=java
   autocmd BufEnter *.zsh-theme setlocal filetype=zsh
   autocmd BufEnter Makefile setlocal noexpandtab
   autocmd BufEnter *.sh setlocal tabstop=2
   autocmd BufEnter *.sh setlocal shiftwidth=2
   autocmd BufEnter *.sh setlocal softtabstop=2
augroup END

" NERDTree.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
let g:NERDTreeDirArrowExpandable = ''
let g:NERDTreeDirArrowCollapsible = ''
nnoremap <C-m> :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-f> :NERDTreeToggle<CR>
